<div class="my-10">
    <div class="card shadow-lg w-75 mx-auto">
        <div class="card-header bg-grey-700">
            <div class="card-title  text-white">Historique des connexions</div>
            <div class="card-toolbar"></div>
        </div>
        <div class="card-body">
            <p class="fw-bold fs-3">Le tableau suivant indique les différentes actions effectuées en relation avec votre compte Vortech Studio.</p>
            <livewire:user-device-table />
        </div>
    </div>
</div>
