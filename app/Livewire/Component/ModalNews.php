<?php

namespace App\Livewire\Component;

use Livewire\Component;

class ModalNews extends Component
{
    public function render()
    {
        return view('livewire.component.modal-news');
    }
}
